module.exports = {
  lintOnSave: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/sass/utils/_mixins.scss";
          @import "@/sass/utils/_variables.scss";`,
      },
    },
  },
};
