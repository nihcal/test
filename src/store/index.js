import Vue from "vue";
import Vuex from "vuex";
import searchVacancy from "./modules/search/vacancy.js";
import searchCompany from "./modules/search/company.js";
import searchResume from "./modules/search/resume.js";
import vacancy from "./modules/vacancy/vacancy.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    searchVacancy,
    searchCompany,
    searchResume,
    vacancy,
  },
});
