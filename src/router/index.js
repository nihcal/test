import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      {
        path: "/job",
        name: "Job",
        component: () => import("../views/job/Job.vue"),
      },
      {
        path: "/search",
        name: "Search",
        component: () => import("../views/Search.vue"),
        children: [
          {
            path: "vacancies",
            name: "SearchVacancy",
            component: () => import("../views/search/Vacancy.vue"),
          },
          {
            path: "resume",
            name: "SearchResume",
            component: () => import("../views/search/Resume.vue"),
          },
          {
            path: "companies",
            name: "SearchCompany",
            component: () => import("../views/search/Company.vue"),
          },
        ],
      },
      {
        path: "/autoraising",
        name: "Autoraising",
        component: () => import("../views/autoraising/Autoraising.vue"),
      },
      {
        path: "/vacancies/:id",
        name: "Vacancy",
        props: true,
        component: () => import("../views/vacancy/Show.vue"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
