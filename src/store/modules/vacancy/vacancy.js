import { VacancyService } from "@/service/index.js";

const state = () => ({
  vacancy: {},
});

const getters = {};

const actions = {
  fetchVacancy({ commit }, vacancy) {
    VacancyService.getVacancy(vacancy)
      .then((response) => {
        commit("SET_VACANCY", response.data);
      })
      .catch(() => {
        alert("Запустите JSON Server, README.md");
      });
  },
};

const mutations = {
  SET_VACANCY(state, vacancy) {
    state.vacancy = vacancy;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
