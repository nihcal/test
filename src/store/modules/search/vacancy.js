import { VacancyService } from "@/service/index.js";

const state = () => ({
  filters: {},
  vacancies: [],
});

const getters = {};

const actions = {
  fetchFilters({ commit }) {
    VacancyService.getFilters()
      .then((response) => {
        commit("SET_FILTERS", response.data);
      })
      .catch(() => {
        alert("Запустите JSON Server, README.md");
      });
  },
  fetchVacancies({ commit, dispatch }) {
    VacancyService.getVacancies()
      .then((response) => {
        commit("SET_VACANCIES", response.data);
        dispatch("fetchFilters");
      })
      .catch(() => {
        alert("Запустите JSON Server, README.md");
      });
  },
};

const mutations = {
  SET_FILTERS(state, filters) {
    state.filters = filters;
  },
  SET_VACANCIES(state, vacancies) {
    state.vacancies = vacancies;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
