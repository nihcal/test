import axios from "axios";
import NProgress from "nprogress";
const apiClient = axios.create({
  baseURL: "http://localhost:3000",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Location: "http://localhost:3000",
  },
});

apiClient.interceptors.request.use((config) => {
  NProgress.start();
  return config;
});

apiClient.interceptors.response.use(
  (response) => {
    NProgress.done();
    return response;
  },
  function(error) {
    NProgress.done();
    return Promise.reject(error);
  }
);

export class VacancyService {
  static getFilters() {
    return apiClient.get("/vacancy-filters");
  }
  static getVacancies() {
    return apiClient.get("/vacancies");
  }
  static getVacancy(vacancy) {
    return apiClient.get(`/vacancies/${vacancy}`);
  }
}
